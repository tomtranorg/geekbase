<?php
/**
 * @file
 * my_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function my_content_type_node_info() {
  $items = array(
    'my_content_type' => array(
      'name' => t('My Content Type'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
